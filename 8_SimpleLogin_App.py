# Introduction to LOGINS in Python
# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
# See PyCharm help at https://www.jetbrains.com/help/pycharm/

""""
A Simple Login program with 3 Attempts using While Loop
"""

pass_word = "Password"
passw = ""
passw_count = 0
passw_limited = 3
out_of_try = False

print()


while passw != pass_word and not(out_of_try):
    if passw_count < passw_limited:
        passw = input("Enter Password: ")
        passw_count += 1
    else:
        out_of_try = True

if out_of_try:
    print()
    print("Maximum allowed Attempts of 3 finished")
else:
    print()
    print("Login Successful!")
