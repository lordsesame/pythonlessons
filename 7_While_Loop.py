# Introduction to WHILE LOOP in Python
# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
# See PyCharm help at https://www.jetbrains.com/help/pycharm/


"""" While loop """

print()
test_loop = int(input("Enter a number less or equal to 10: "))

while test_loop <= 10:
    print(test_loop)
    test_loop += 1
else:
    print()
    print("End of loop")
